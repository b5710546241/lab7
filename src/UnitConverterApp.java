/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * Application class for invoking UnitConverter and GUI.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.03
 */
public class UnitConverterApp {

	/**
	 * Initialize UnitConverter and GUI.
	 * @param args is not used
	 */
	public static void main(String[] args){
		UnitConverter uc = new UnitConverter();
		ConverterUI converter = new ConverterUI( uc );
	}
}
