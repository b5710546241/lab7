/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Reveal the converted value in GUI.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.03
 */
public class ConverterUI extends JFrame {

	private UnitConverter unitconverter;
	private JTextField inputField; 
	private JComboBox comboBox1;
	private JTextField outputField;
	private JComboBox comboBox2;
	private JButton convertButton;
	private JButton clearButton;

	/**
	 * Constructor which initialize UnitConverter, setting JFrame properties,
	 * and invoking initComponents.
	 * @param uc is an UnitConverter
	 */
	public ConverterUI( UnitConverter uc) {
		this.unitconverter = uc;
		this.setTitle("Simple Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents();
		this.pack();	
	}

	/**
	 * Creating the GUI components.
	 */
	public void initComponents() {
		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );
		inputField = new JTextField(10);
		contents.add( inputField );
		ActionListener enter = new ConvertButtonListener();
		inputField.addActionListener(enter);
		comboBox1 = new JComboBox( unitconverter.getUnits() );
		contents.add( comboBox1 );
		JLabel label = new JLabel( " = " );
		contents.add( label );
		outputField = new JTextField(10);
		outputField.setEditable(false);
		contents.add( outputField );
		comboBox2 = new JComboBox( unitconverter.getUnits() );
		contents.add( comboBox2 );

		convertButton = new JButton("Convert");
		contents.add( convertButton );
		ActionListener convertListener = new ConvertButtonListener( );
		convertButton.addActionListener( convertListener );

		clearButton = new JButton("Clear");
		contents.add( clearButton );
		ActionListener clearListener = new ClearButtonListener( );
		clearButton.addActionListener( clearListener );
		this.setVisible(true);
	}

	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed. */
		public void actionPerformed( ActionEvent evt ) {
			String s = inputField.getText().trim();
			//This line is for testing.  Comment it out after you see how it works.
			//System.out.println("actionPerformed: input=" + s);
			try {
				double value = Double.valueOf( s );
				if(value<0)
					throw new NumberFormatException();
				Unit unit1 = (Unit)comboBox1.getSelectedItem();
				Unit unit2 = (Unit)comboBox2.getSelectedItem();
				double result = unitconverter.convert( value, unit1, unit2);
				outputField.setText(String.format("%.6f", result));

			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Please input valid number", null, JOptionPane.ERROR_MESSAGE);
			}
		}
	}  // end of the inner class

	class ClearButtonListener implements ActionListener {

		public void actionPerformed( ActionEvent evt ) {
			inputField.setText("");
			outputField.setText("");
		}
	}

}
