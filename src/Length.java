/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * Convert a value from one length unit to another.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.03
 */
public enum Length implements Unit {

	METER("Meters", 1.0),
	KILOMETER("Kilometers", 1000.0),
	MILE("Miles", 1609.344),
	FOOT("Feet", 0.30480),
	WA("Waaaa", 2.0);
	
	private final double value;
	private final String name;
	
	private Length(String name, double value) {
		this.value = value;
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}

	@Override
	public double convertTo(Unit unit, double amount) {
		return amount*value/unit.getValue();
	}

	@Override
	public double getValue() {
		return this.value;
	}
	
}
