/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

/**
 * Receives requests from the UI to converts a value from one unit to another.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @version 2015.03.03
 */
public class UnitConverter {

	/**
	 * Converting from one unit to another unit.
	 * @param amount is an input from UI
	 * @param fromUnit is an unit from comboBox1
	 * @param toUnit is an unit from comboBox2
	 * @return double ( converted amount )
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit){
		return fromUnit.convertTo(toUnit, amount);
	}
	
	/**
	 * Getting the values of units.
	 * @return array of units in enum
	 */
	public Unit[] getUnits(){
		return Length.values();
	}
}
